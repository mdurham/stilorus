import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stilorus',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Stilorus'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  Offset offset = Offset.zero;

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    Expanded panel = Expanded(
      child: AspectRatio(
        aspectRatio: 1.0,
        child: Container(
          height: 10.0,
          width: 10.0,
          color: Colors.amber,
        ),
      )
    );
    Expanded window = Expanded(
      child: AspectRatio(
        aspectRatio: 1.0,
        child: Container(
          height: 10.0,
          width: 10.0,
          color: Colors.transparent,
        ),
      )
    );
    Column outerColumn = Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        panel,
        panel,
        panel,
      ],
    );
    Column innerColumn = Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        panel,
        window,
        panel,
      ],
    );
    Row maskRow = Row(children: <Widget>[
      outerColumn,
      innerColumn,
      outerColumn,
    ],);
    List<Expanded> tile = [];
    for (var i = 1; i <= 16; i++) {
      tile.add(
        Expanded(
          child: AspectRatio(
            aspectRatio: 1.0,
            child: Container(
              height: 30.0,
              width: 30.0,
              decoration: BoxDecoration(
                border:Border.all(
                  color: Colors.black,
                  width: 1.0,
                ),
                color: Colors.yellow,
              ),
              child: Center(
                child: Text(
                  i.toString(),
                  style: TextStyle(color: Colors.black, fontSize: 22.0),
                ),
              ),
            ),
          ),
        )
      );
    }
    List<Column> column = [];
    for (var i = 0; i < 4; i++) {
      column.add(
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            tile[i],
            tile[i+4],
            tile[i+8],
            tile[i+12],
            tile[i],
            tile[i+4],
            tile[i+8],
            tile[i+12],
            tile[i],
            tile[i+4],
            tile[i+8],
            tile[i+12],
          ],
        )
      );
    }
    List<Row> row = [];
    for (var i = 0; i < 4; i++) {
      row.add(
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            tile[4*i],
            tile[4*i+1],
            tile[4*i+2],
            tile[4*i+3],
            tile[4*i],
            tile[4*i+1],
            tile[4*i+2],
            tile[4*i+3],
            tile[4*i],
            tile[4*i+1],
            tile[4*i+2],
            tile[4*i+3],
          ]
        )
      );
    }
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: GestureDetector(
        onTapDown: (TapDownDetails details) {
          print('Tap Down: ' + details.toString());
          print('Global position: ' + details.globalPosition.toString());
        },
        onHorizontalDragUpdate: (DragUpdateDetails details) {
          print('Horizontal Drag Update: ' + details.toString());
          setState(() {
            offset = Offset(offset.dx + details.primaryDelta, offset.dy);
          });
        },
        onVerticalDragUpdate: (DragUpdateDetails details) {
          print('Vertical Drag Update: ' + details.toString());
          setState(() {
            offset = Offset(offset.dx, offset.dy + details.primaryDelta);
          });
        },
        child: Center(
          child: AspectRatio(
            aspectRatio: 1.0,
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                OverflowBox(
                  child: Transform.scale(
                    scale: 3.0,
                    child: Row(
                      children: <Widget>[
                        column[0],
                        column[1],
                        column[2],
                        column[3],
                        column[0],
                        column[1],
                        column[2],
                        column[3],
                        column[0],
                        column[1],
                        column[2],
                        column[3],
                      ],
                    ),
                  ),
                ),
                OverflowBox(
                  child: Transform.scale(
                    scale: 3.0,
                    child: Column(
                      children: <Widget>[
                        row[0],
                        row[1],
                        row[2],
                        row[3],
                        row[0],
                        row[1],
                        row[2],
                        row[3],
                        row[0],
                        row[1],
                        row[2],
                        row[3],
                      ],
                    ),
                  ),
                ),
                OverflowBox(
                  child: Transform.scale(
                    scale: 3.0,
                    child: maskRow,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
