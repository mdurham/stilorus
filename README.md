# stilorus = Sliding TILe tORUS

Stilorus (\\ sti-ˈlȯr-əs \\) is a sliding tile puzzle on a flattened torus inspired by the famous "15" puzzle.
It has been created as an entry into the 2019 [Flutter Create](https://flutter.dev/create) contest (\#FlutterCreate).

## Date
This documentation was valid on April 6, 2019.

## Environment

Stilorus is written in pure Dart code and only imports 'dart:math' and 'package:flutter/material.dart'.
The app will build and run on both iOS and Android devices in both landscape and portrait orientations. The code exists in two forms:

1. The [original development code](https://bitbucket.org/mdurham/stilorus)
2. The official entry code, reduced in size from the original to meet the Flutter Create size limit of no more than 5120 bytes

## Getting Started

Stilorus opens to its only page, the puzzle grid:

![Puzzle grid](https://www.RedQuark.com/Stilorus_256x512.png "Stilorus Puzzle Grid")

Repeatedly slide rows of tiles to the left and right, alternately followed by sliding columns of tiles up or down, until the order of the numbers on the grid has been scrambled.
Then proceed to "solve" the puzzle by restoring the grid to its initial configuration with the numbers in order.

## Architectural Notes

* The model for the puzzle is a simple list of integers along with methods for shifting rows left and right and columns up and down
	* The shifts wrap from top to bottom, bottom to top, left to right, and right to left like on a torus
* There are two views: a static view and a dynamic view for when the tiles are sliding
* Although you only see a 4x4 grid, there are really two 12x12 grids — one on top of the other in a Stack
	* Having a 12x12 grid allows Flutter to initially layout the grid on its own in a centered square that fills the smaller dimension
    	* One grid consists of a Row of 12 Columns of 12 Containers
	    	* The four middle Columns each have a GlobalKey so they can "slide" vertically
	  	* One grid consists of a Column of 12 Rows of 12 Containers
	    	* The four middle Rows each have a GlobalKey so they can "slide" horizontally
   	* Using a FractionallySizedBox with width and height factors of 3.0 means that only the center-third of the grid fits in the centered square
   	* A 3x3 mask of squares with a transparent center overlays the 12x12 grids to hide all but the center 4x4 squares of the 12x12 grids
    	* The mask uses an OverflowBox and a Transform with a scale of 3.0 to match the mask's size to that of the 12x12 grids
* All interactions are handled by Drag GestureDectectors
* The dynamic view is constructed of three layers in a Stack
   	* The bottom layer is the static 12x12 grid, but in a SizedBox in a Center widget this time
    	* SizedBox uses the dimensions of the center square of the mask of the static view at the time the Drag starts for its dimensions
   	* The middle layer is the Row or Column underneath the Drag in a SizedBox
    	* The SizedBox is in a Positioned widget that tracks the Drag
   	* The top layer is the mask again, but like the 12x12 grid, in a SizedBox in a Center widget this time
    	* SizedBox uses the dimensions of the center square of the mask of the static view at the time the Drag starts for its dimensions
* When the Drag is complete, the model is adjusted, and the static view is rebuilt from the model

## License (MIT)

Copyright 2019 Mark A. Durham

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Credits

* [Original code](https://bitbucket.org/mdurham/stilorus) by Mark A. Durham
* Original code reduced to minified form by Christopher A. Durham